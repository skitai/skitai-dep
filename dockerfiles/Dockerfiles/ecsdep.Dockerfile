ARG DOCKER_TAG=latest
FROM docker:${DOCKER_TAG} as base

ARG DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1

LABEL title="AWS Cli, Terraform and Docker Compose"
LABEL author="hansroh"
LABEL version="1.0"

ENV LC_ALL=C.UTF-8

RUN apk add --update --no-cache python3 curl jq zip bash py3-pip
# RUN apk add --update --no-cache docker-cli docker-cli-compose
RUN mkdir -p /root/.config/pip && printf "[global]\nbreak-system-packages = true\n" > /root/.config/pip/pip.conf

RUN wget -O terraform.zip https://releases.hashicorp.com/terraform/1.3.5/terraform_1.3.5_linux_amd64.zip
RUN unzip terraform.zip && rm terraform.zip
RUN mv terraform /usr/bin/terraform

RUN apk add --no-cache --virtual .build-deps build-base linux-headers python3-dev && \
    pip3 install awscli python-telegram-bot click psutil && \
    apk del .build-deps

COPY include/telegram.py /usr/bin/telegram
RUN chmod +x /usr/bin/telegram

COPY include/wait-for-it.sh /usr/bin/wait-for-it.sh
RUN chmod +x /usr/bin/wait-for-it.sh

FROM base as image-dind
RUN apk add tmux git
RUN pip3 install -U pytest

FROM base as image-latest
