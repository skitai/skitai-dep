#! /bin/bash
set -x

cd ../example-app/dep

export SERVICE_STAGE=qa
echo "Shutting down QA services ..."
ecsdep service down --yes



export SERVICE_STAGE=production
echo "Shutting down shutdown services ..."
ecsdep -f compose.worker.yml service down --yes

echo "Shutting down maintain services ..."
ecsdep -f compose.maintain.yml service down --yes

echo "Shutting down production services ..."
ecsdep service down --yes
ecsdep -f compose.ecs.novpc.yml service down --yes
ecsdep -f compose.ecs.noec2.yml service down --yes


if [ "$1" == "--cluster" ]
then
    echo "Destroy ECS cluster..."
    ecsdep cluster destroy --yes
    ecsdep -f compose.ecs.novpc.yml cluster destroy --yes
    ecsdep -f compose.ecs.noec2.yml cluster destroy --yes
fi
