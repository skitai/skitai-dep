import sys; sys.path.insert (0, "../")
from ecsdep.terraform import Terraform, Tasks
import os
import subprocess
import yaml
import pytest
import json
import re
from pprint import pprint

if not os.path.exists (os.path.expanduser ("~/.ecsdep")):
    os.symlink (os.path.abspath ("../ecsdep/templates"), os.path.expanduser ("~/.ecsdep"), True)

def run (cmd):
    return subprocess.run (cmd, shell = True, capture_output = True)

@pytest.fixture
def data ():
    with open ("../example-app/dep/compose.ecs.yml") as f:
        return f.read ()

@pytest.fixture
def data_noec2 ():
    with open ("../example-app/dep/compose.ecs.noec2.yml") as f:
        return f.read ()

RX_SPACES = re.compile (r"\s+")
def test_utils (data):
    cli = Terraform (data, disable_terrform = True)
    assert RX_SPACES.sub ("", cli.to_tfdict ({"a": 1, "b": "c"})) == '{a=1b="c"}'

    k = RX_SPACES.sub ("", cli.to_tfdict ({"a": 1, "b": 'c'}))
    assert k == '{a=1b="c"}'

    k = cli.to_tfdict ({"a": 1, "b": 'c'})
    assert cli.to_tfdict ({"k": k}, 4).count ('"') == 2
    assert cli.to_tflist (['1', "2", 3]) == '["1", "2", 3]'

    k = RX_SPACES.sub ("", cli.to_tfdict ({"a": 1, "b": ['1']}))
    assert k == '{a=1b=["1"]}'



# cluster tf =================================
def render_cluster (mod):
    out = yaml.dump (mod)
    cli = Terraform (out, path = '/my/config/path/to.yml', disable_terrform = True)
    return cli.generate_cluster_declares ()

def test_cluster_convert (data):
    cli = Terraform (data, path = '/my/config/path/to.yml', disable_terrform = True)
    d = {}
    cli.set_terraform_vars (d)
    out_tf = cli.generate_cluster_declares ()
    assert out_tf.count ("variable") == 11
    assert "/my/config/path/ecsdep.pub" in out_tf

def test_cluster_vpc (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    assert mod ['x-ecs-cluster']['vpc']['cidr_block']
    assert 'cidr_block = "10.0.0.0/16"' in render_cluster (mod)
    assert 'peering_vpc_ids = ["default"]' in render_cluster (mod)
    assert 'default = ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]' in render_cluster (mod)

    del mod ['x-ecs-cluster']['vpc']
    del mod ['x-ecs-cluster']['task-iam-policies']
    assert 'cidr_block = ""' in render_cluster (mod)
    assert 'peering_vpc_ids = []' in render_cluster (mod)
    assert 'variable "task_iam_policies" {\n  default = []' in render_cluster (mod)

def test_cluster_noec2 (data_noec2):
    mod = yaml.load (data_noec2, Loader=yaml.FullLoader)
    tf = render_cluster (mod)
    assert "min = 0" in tf
    assert 'desired = 0' in tf
    assert '"cert_name" {\n  default = ""' in tf
    assert '"ami" {\n  default = ""' in tf
    assert '"public_key_file" {\n  default = ""' in tf
    print (tf)

# service tf =================================
def render_service (mod):
    out = yaml.dump (mod)
    cli = Terraform (out, disable_terrform = True)
    return cli.generate_service_declares ("qa", "latest") ["declares.tf"]

def test_create_service_convert (data):
    try: del os.environ ["SERVICE_STAGE"]
    except KeyError: pass

    cli = Terraform (data, disable_terrform = True)
    with pytest.raises (AssertionError):
        cli.generate_tasks ("qa", "latest")

    os.environ ["SERVICE_STAGE"] = "qa"
    cli = Terraform (data, disable_terrform = True)
    out_tf = cli.generate_service_declares ("qa", "latest")
    assert out_tf ["declares.tf"].count ("variable") == 15
    assert 'default = ["EC2"]' in out_tf ["declares.tf"]

def test_ports (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["services"]["skitai-app"]["ports"]
    del mod ["services"]["skitai-nginx"]["ports"]
    del mod ["x-ecs-service"]["deploy"]["compatibilities"]

    os.environ ["SERVICE_STAGE"] = "qa"
    with pytest.raises (AssertionError):
        out_tf = render_service (mod)

    del mod ["x-ecs-service"]["loadbalancing"]
    out_tf = render_service (mod)
    assert '"exposed_container" {\n  default = []' in out_tf
    assert 'default = ["EC2"]' in out_tf

    mod ["x-ecs-service"]["deploy"]["compatibilities"] = ["external"]
    out_tf = render_service (mod)
    assert '"exposed_container" {\n  default = []' in out_tf
    assert 'default = ["EXTERNAL"]' in out_tf


    mod ["x-ecs-service"]["deploy"]["compatibilities"] = ["ec2"]
    mod ["services"]["skitai-nginx"]["ports"] = ["80:80"]
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"]["compatibilities"] = ["ec2", "fargate"]
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"]["compatibilities"] = ["x-ec2"]
    with pytest.raises (AssertionError):
        render_service (mod)

    del mod ["services"]["skitai-nginx"]["ports"]
    del mod ["x-ecs-service"]["deploy"]
    with pytest.raises (AssertionError):
        render_service (mod)

def test_service_resource_limits (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    mod ["x-ecs-service"]["deploy"]["compatibilities"] = ["fargate"]
    del mod ["x-ecs-service"]["deploy"]["resources"]
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"]["resources"] = {
        "limits": {"cpus": "1024", "memory": 512}
    }
    with pytest.raises (TypeError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"]["resources"] = {
        "limits": {"cpus": 1024, "memory": "512M"}
    }
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"]["resources"] = {
        "limits": {"cpus": "1024", "memory": "512M"}
    }
    mod ["x-ecs-service"]["deploy"]["resources"]["limits"]["memory"] = "2048M"
    assert 'default = ["FARGATE"]' in render_service (mod)

    del mod ["services"]["skitai-app"]["deploy"]
    del mod ["services"]["skitai-nginx"]["deploy"]
    del mod ["x-ecs-service"]["deploy"]
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["services"]["skitai-app"]["deploy"] = {"resources": {"reservations": {"memory": "10M"}}}
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["x-ecs-service"]["deploy"] = {}
    mod ["x-ecs-service"]["deploy"]["resources"] = {
        "limits": {"cpus": "1024", "memory": "512M"}
    }
    mod ["services"]["skitai-nginx"]["deploy"] = {"resources": {"reservations": {"memory": "10M"}}}
    render_service (mod)

    del mod ["services"]["skitai-app"]["deploy"]
    del mod ["services"]["skitai-nginx"]["deploy"]
    mod ["x-ecs-service"]["deploy"] = {"resources": {"limits": {"memory": "10M"}}}
    with pytest.raises (AssertionError):
        render_service (mod)
    mod ["services"]["skitai-nginx"]["deploy"] = None
    render_service (mod)

    mod ["services"]["skitai-nginx"]["deploy"] = {"resources": {"reservations": {"memory": "128M"}}}
    mod ["x-ecs-service"]["deploy"] = {"resources": {"limits": {"memory": "100M"}}}
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["services"]["skitai-nginx"]["deploy"] = {"resources": {"reservations": {"memory": "128M", "cpus": "1024"}}}
    mod ["x-ecs-service"]["deploy"] = {"resources": {"limits": {"memory": "100M", "cpus": "24"}}}
    with pytest.raises (AssertionError):
        render_service (mod)

    mod ["services"]["skitai-nginx"]["deploy"] = {"resources": {"reservations": {"memory": "128M", "cpus": "1024"}}}
    mod ["x-ecs-service"]["deploy"] = {"resources": {"limits": {"memory": "128M", "cpus": "0"}}}
    render_service (mod)

def test_autoscaling (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["x-ecs-service"]["deploy"]["autoscaling"]

    out_tf = render_service (mod)
    assert "cpu = 0" in out_tf
    assert "memory = 0" in out_tf

def test_stages (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["x-ecs-service"]["stages"]
    assert 'env_service_stage = "production"' in render_service (mod)

    mod ["x-ecs-service"]["stages"] = {"default": {"env-service-stage": "qa", "autoscaling": {"min": 5, "max": 7}}}
    assert 'min = 5' in render_service (mod)
    assert 'max = 7' in render_service (mod)



# tasks json =================================
def render_tasks (mod):
    out = yaml.dump (mod)
    cli = Terraform (out, disable_terrform = True)
    tasks = cli.generate_tasks ("qa", "latest")
    return json.loads (tasks.out_json), tasks.load_balancers, tasks.loggings

def test_generate_tasks (data):
    cli = Terraform (data, disable_terrform = True)

    os.environ ["SERVICE_STAGE"] = "qa"
    cli = Terraform (data, disable_terrform = True)
    _tasks = cli.generate_tasks ("qa", "latest")
    assert len (_tasks.load_balancers) == 1
    assert len (_tasks.loggings) == 2
    tasks = json.loads (_tasks.out_json)

    assert len (tasks [0]["environment"]) == 1
    assert tasks [0]["environment"][0]["value"] == "qa"
    assert len (tasks [0]["secrets"]) == 1
    assert tasks [0]["secrets"][0]["name"] == "REGISTRY_USER"
    assert tasks [0]["secrets"][0]["valueFrom"][:4] == "arn:"

    assert tasks [0]["repositoryCredentials"]
    assert "memoryReservation" not in tasks [0]
    assert "memory" not in tasks [0]
    assert "cpu" not in tasks [0]
    assert tasks [0]["healthCheck"]["command"][0] == "CMD-SHELL"
    assert not tasks [0]["dependsOn"]
    assert tasks [0]["portMappings"][0]["hostPort"] == 0
    assert tasks [0]["portMappings"][0]["containerPort"] == 5000
    assert tasks [0]["logConfiguration"]["options"]["awslogs-group"] == "/ecs/normal/service--qa/skitai-app"
    assert tasks [0]["essential"]
    assert "resourceRequirements" not in tasks [0]

    assert tasks [1]["dependsOn"][0]["containerName"] == "skitai-app"
    assert tasks [1]["dependsOn"][0]["condition"] == "HEALTHY"
    assert tasks [1]["portMappings"][0]["hostPort"] == 0
    assert tasks [1]["portMappings"][0]["containerPort"] == 80
    assert tasks [1]["links"] == ["skitai-app"]
    assert "memoryReservation" not in tasks [1]
    assert "memory" not in tasks [1]
    assert "cpu" not in tasks [1]
    assert len (tasks [1]["secrets"]) == 0


def test_generate_noec2_tasks (data_noec2):
    cli = Terraform (data_noec2, disable_terrform = True)

    os.environ ["SERVICE_STAGE"] = "qa"
    cli = Terraform (data_noec2, disable_terrform = True)
    _tasks = cli.generate_tasks ("qa", "latest")
    assert len (_tasks.load_balancers) == 0


def test_essential (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["services"]["skitai-app"]["x-ecs-essential"]
    del mod ["services"]["skitai-nginx"]["deploy"]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert len (tasks) == 1
    assert len (loggings) == 1
    assert len (load_balancer) == 0
    assert tasks [0]["essential"] is True

def test_memory_unit (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    mod ["services"]["skitai-app"]["deploy"] = {"resources": {"reservations": {"memory": 16}}}
    with pytest.raises (TypeError):
        render_tasks (mod)

def test_memory_resources (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    mod ["services"]["skitai-app"]["deploy"] = {"resources": {"reservations": {"memory": "16M"}}}
    mod ["services"]["skitai-app"]["deploy"]["resources"]["limits"] = {"memory": "16M"}
    with pytest.raises (AssertionError):
        render_tasks (mod)

    mod ["services"]["skitai-nginx"]["deploy"] = {"resources": {"reservations": {"memory": "8M"}}}
    tasks = render_tasks (mod) [0]
    assert tasks [0]["memory"] == 16
    assert tasks [0]["memoryReservation"] == 16

def test_loggings (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["services"]["skitai-app"]["logging"]["x-ecs-driver"]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert len (tasks) == 2
    assert len (loggings) == 1
    assert len (load_balancer) == 1

def test_healthcheck (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    del mod ["services"]["skitai-app"]["healthcheck"]
    with pytest.raises (AssertionError):
        tasks, load_balancer, loggings = render_tasks (mod)

def test_loadbalancer (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    mod ["services"]["skitai-nginx"]["ports"] = ["80"]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert not load_balancer

    del mod ["x-ecs-service"]["loadbalancing"]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert not load_balancer

    os.environ ["SERVICE_STAGE"] = "qa"
    cli = Terraform (yaml.dump (mod), disable_terrform = True)
    out_tf = cli.generate_service_declares ("qa", "latest")
    assert "target_group" in out_tf ["declares.tf"]

def test_gpu (data):
    mod = yaml.load (data, Loader=yaml.FullLoader)
    mod ["services"]["skitai-app"]["deploy"] = {"resources": {"reservations": {"x-ecs-gpus": 2}}}
    tasks, load_balancer, loggings = render_tasks (mod)
    assert tasks [0]["resourceRequirements"][0]["type"] == "GPU"
    assert tasks [0]["resourceRequirements"][0]["value"] == 2

    del mod ["services"]["skitai-app"]["deploy"]["resources"]["reservations"]["x-ecs-gpus"]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert "resourceRequirements" not in tasks [0]

    mod ["services"]["skitai-app"]["deploy"]["resources"]["reservations"]["devices"] = [dict (
        driver = "nvidia",
        count = 1,
        capabilities = ["gpu"]
    )]
    tasks, load_balancer, loggings = render_tasks (mod)
    assert tasks [0]["resourceRequirements"][0]["type"] == "GPU"
    assert tasks [0]["resourceRequirements"][0]["value"] == 1
