from atila import Atila

def __app__ ():
    return Atila (__name__)

def __mount__ (context, app):
    @app.route ("/")
    def index (context):
        return "<h1>ecsdep</h1>"

    @app.route ("/ping")
    def ping (context):
        context.response.set_header ("Content-Type", "text/plain")
        return "pong"
